python-pyforge (1.4.0-2) unstable; urgency=medium

  * Cherry-pick upstream commit for Python 3.13 compatibility (Closes:
    #1082257).
  * Cherry-pick upstream commit to stop using pkg-resources (Closes:
    #1083690).
  * Update Standards-Version to 4.7.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Thu, 14 Nov 2024 00:33:51 +1100

python-pyforge (1.4.0-1) unstable; urgency=medium

  * New upstream release.
    - replace nose with pytest and tox
    - add pbr as a build system
    - drop all patches (applied upstream)
    - add new patch to fix syntax in setup.py
    - add new patch to fix tool invocation in tox.ini

 -- Stuart Prescott <stuart@debian.org>  Sun, 22 Oct 2023 00:37:52 +1100

python-pyforge (1.3.0-8) unstable; urgency=medium

  * Minor packaging updates from lintian-brush.

 -- Stuart Prescott <stuart@debian.org>  Mon, 30 Jan 2023 16:52:29 +1100

python-pyforge (1.3.0-7) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Stuart Prescott ]
  * Update Standards-Version to 4.5.1 (no changes required).
  * Update to debhelper-compat (= 13).
  * Add Rules-Requires-Root: no.
  * Update watch file format.
  * Add patch to silence DeprecationWarning in tests.

 -- Stuart Prescott <stuart@debian.org>  Mon, 21 Dec 2020 12:38:25 +1100

python-pyforge (1.3.0-6) unstable; urgency=medium

  * Only test with py3versions -s
  * Update Standards-Version to 4.5.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Tue, 24 Mar 2020 02:11:15 +1100

python-pyforge (1.3.0-5) unstable; urgency=medium

  * Drop Python 2 package python-forge
  * Update to debhelper 12
  * Update Standards-Version to 4.4.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Thu, 08 Aug 2019 00:59:14 +1000

python-pyforge (1.3.0-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/copyright: Use https protocol in Format field

  [ Stuart Prescott ]
  * Update Standards-Version to 4.3.0 (no changes required).
  * Use debhelper-compat (= 11)

 -- Stuart Prescott <stuart@debian.org>  Sun, 27 Jan 2019 00:03:06 +1100

python-pyforge (1.3.0-3) unstable; urgency=medium

  * Add python3-all to dependencies for autopkgtest tests.

 -- Stuart Prescott <stuart@debian.org>  Tue, 24 Oct 2017 20:08:08 +1100

python-pyforge (1.3.0-2) unstable; urgency=medium

  * Cherry pick upstream patch to fix a test that fails on 32-bit architectures.

 -- Stuart Prescott <stuart@debian.org>  Fri, 25 Nov 2016 01:20:04 +1100

python-pyforge (1.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Change d/watch to point to github.
  * Update d/copyright.
  * Switch to debhelper compat 10.
  * Add autopkgtest tests.
  * Lots of small packaging improvements.
  * Add package to collab-maint.

 -- Stuart Prescott <stuart@debian.org>  Fri, 18 Nov 2016 02:06:20 +1100

python-pyforge (1.2.0-1) unstable; urgency=low

  * Initial packaging.

 -- Stuart Prescott <stuart@debian.org>  Fri, 27 Nov 2015 07:53:57 +1100
